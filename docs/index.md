# Kivy documents from SK-GLOBAL

[![PyPI](https://img.shields.io/pypi/v/kivy.svg?style=for-the-badge&logo=kivy)](https://kivy.org/)

[![GitHub issues](https://img.shields.io/github/issues/kivy/kivy.svg?logo=github&style=flat-square)](https://github.com/kivy/kivy/issues)
[![GitHub forks](https://img.shields.io/github/forks/kivy/kivy.svg?logo=github&style=flat-square)](https://github.com/kivy/kivy/network)
[![GitHub stars](https://img.shields.io/github/stars/kivy/kivy.svg?logo=github&style=flat-square)](https://github.com/kivy/kivy/stargazers)
[![GitHub license](https://img.shields.io/github/license/kivy/kivy.svg?logo=github&style=flat-square)](https://github.com/kivy/kivy/blob/master/LICENSE)

[![Coverage Status](https://coveralls.io/repos/kivy/kivy/badge.svg?branch=master)](https://coveralls.io/r/kivy/kivy?branch=master)
[![Build Status](https://travis-ci.org/kivy/kivy.svg?branch=master)](https://travis-ci.org/kivy/kivy)
[![Build status](https://ci.appveyor.com/api/projects/status/sqc46n4a3bq2gj1s/branch/master?svg=true)](https://ci.appveyor.com/project/KivyOrg/kivy/branch/master)

[![Bountysource](https://www.bountysource.com/badge/tracker?tracker_id=42681)](https://www.bountysource.com/trackers/42681-kivy?utm_source=42681&utm_medium=shield&utm_campaign=TRACKER_BADGE)
[![Backers on Open Collective](https://opencollective.com/kivy/backers/badge.svg)](https://opencollective.com/kivy)
[![Sponsors on Open Collective](https://opencollective.com/kivy/sponsors/badge.svg)](https://opencollective.com/kivy)

[**Kivy's wiki official page**](https://github.com/kivy/kivy/wiki)

---

## Kivy introduction

![Not found](img/introduction2.png)

Kivy - Open source Python library for rapid development of applications
that make use of innovative user interfaces, such as multi-touch apps.

In this document, we will focus to how to build simple app and deploy on multi platform.
If you have found any issue on this document, please feedback to us by email python@sk-global.biz

Thank you for viewing this document.

---

## Environment preparing

In this session, we will setup python env, git and source manager.

### 1. Windows ENV <small>- [view official](https://kivy.org/doc/stable/installation/installation-windows.html)</small>

- [Install Python 3.7](https://www.python.org/downloads/) (and 2.7 if need)

    <!-- - Download exe file -->
    <!-- ![Not found](img/download-python.png) -->

    - When run the installation file, should check *'Add Python 3.7 to PATH'*.
    - To test python, open Windows Powershell and run command `py -3 -V` (this must show python's version).

- [Install Git](https://git-scm.com/downloads)

    <!-- ![Not found](img/download-git.png) -->
    Git is a version-control system for tracking changes in computer files
    and coordinating work on those files among multiple people

- [Install Sourcetree](https://www.sourcetreeapp.com/)

    <!-- ![Not found](img/download-sourcetree.png) -->
    Sourcetree simplifies how you interact with your Git repositories so you can focus on coding.
    Visualize and manage your repositories through Sourcetree's simple Git GUI.

    <!-- - Use google account xxx@sk-global.biz to login -->

- [Install Visual Studio Code IDE](https://code.visualstudio.com/download):
    <!-- ![Not found](img/download-vscode.png) -->

    - When run the installation file, in step 'Select Additional Tasks'
    click `Add "Open with Code" action to Windows Explorer directory context menu`
    and then click 'Next' and 'Install'.
    - Import custom settings and extensions (looking for a way, it will be more detailed instructions later)

- Install VirtualEnv (isolated environment for python)
    - Run cmd `py -3.7 -m pip install virtualenv`

- To allow running .ps file, run command `Set-ExecutionPolicy RemoteSigned -Scope CurrentUser` in PowerShell, then type "y" and press Enter.

#### Use WSL to build android

- Read [this](https://docs.microsoft.com/en-us/windows/wsl/install-win10) to install.
- Install build libs, and a Java SDK:

    ```bash
    sudo apt update
    sudo apt upgrade -y
    sudo apt install build-essential ccache git libncurses5 libstdc++6 libgtk2.0-0 libpangox-1.0-0 libpangoxft-1.0-0 libidn11 openjdk-8-jdk unzip zip zlib1g-dev zlib1g pkg-config
    ```

- Choose java version 8:

    ```bash
    sudo update-alternatives --config java
    ```

    ```log
    There are 2 choices for the alternative java (providing /usr/bin/java).

      Selection    Path                                            Priority   Status
    ------------------------------------------------------------
    * 0            /usr/lib/jvm/java-11-openjdk-amd64/bin/java      1101      auto mode
      1            /usr/lib/jvm/java-11-openjdk-amd64/bin/java      1101      manual mode
      2            /usr/lib/jvm/java-8-openjdk-amd64/jre/bin/java   1081      manual mode

    Press <enter> to keep the current choice[*], or type selection number:
    ```

- Install python dev and pip:

    ```bash
    sudo apt install python3-pip python3-dev
    ```

- Install Cython and buildozer:

    ```shell
    pip3 install cython git+https://github.com/Thong-Tran/buildozer.git@fix-errors
    ```

**Note:**

  - PATH of WSL include Windows's PATH, it may cause some errors(because of [this feature](https://docs.microsoft.com/en-us/windows/wsl/interop)). You should add these two lines to ~/.profile file:

    ```bash
    unset PATH
    . /etc/environment
    ```

    Or add file /etc/wsl.conf ([see more](https://blogs.msdn.microsoft.com/commandline/2018/02/07/automatically-configuring-wsl/)) with content:

    ```text
    [interop]
    appendWindowsPath = false
    ```

  - Should run `adb start-server` on Windows before run adb on WSL.
  - When build complete, java don't close. You may kill it manually.

### 2. MacOS ENV <small>- [view official](https://kivy.org/doc/stable/installation/installation-osx.html#using-homebrew-with-pip)</small>

- [Install Homebrew](https://brew.sh/)
    - Run this cmd in Terminal

    ```
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    ```

    <!-- ![Not found](img/install-brew.png) -->

- [Install Python 3.7](https://www.python.org/downloads/) (and 2.7 if need)
    <!-- ![Not found](img/download-python-osx.png) -->
    - To test python, open Terminal and run command `python3 -V` (this must show python's version).

- Install XCode from AppStore (you must have Apple account)

    <!-- ![Not found](img/install-xcode.png) -->

- [Install Sourcetree](https://www.sourcetreeapp.com/)
    <!-- ![Not found](img/download-sourcetree-osx.png) -->
    <!-- - Choose 'Bitbucket Cloud' and use google account xxx@sk-global.biz to login -->

- [Install Visual Studio Code IDE](https://code.visualstudio.com/download):
    <!-- ![Not found](img/download-vscode-osx.png) -->
    - The downloaded file will be .app file, so you should copy it to `/Applications` then move to Trash file in Downloads folder.
    - Import custom settings and extensions (looking for a way, it will be more detailed instructions later)

- Install VirtualEnv (isolated environment for python)
    - Run command `pip3 install --upgrade virtualenv`.

- Install the requirements of Kivy (you can skip gstreamer if you aren’t going to use video/audio)

    ```bash
    brew install pkg-config sdl2 sdl2_image sdl2_ttf sdl2_mixer gstreamer
    ```

- Option to pack mobile app:

    - Navigate to `/Applications/Python x.x/` and run in Terminal `Install Certificates.command`.
    - Install: [buildozer fork of Sk-global](https://github.com/Thong-Tran/buildozer/tree/fix-errors), dependent of buildtool:

    ```bash
    wget https://bootstrap.pypa.io/get-pip.py
    python get-pip.py --user
    python -m pip install requests sh pbxproj cffi git+https://github.com/Thong-Tran/buildozer.git@fix-errors -U --user
    rm get-pip.py
    ```

    - The latest java version (default install) will get error when run android sdk, so you need to install Java 8.

    ```bash
    brew tap caskroom/versions
    brew cask install java8
    touch ~/.android/repositories.cfg
    ```

    And add `export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)` to file `~/.bash_profile` or `~/.profile`. Reopen terminal and run `java -version` to test using the correct version.

### 3. Linux ENV

- Please follow tutorial for your Linux OS version:
[Installation on Linux](https://kivy.org/doc/stable/installation/installation-linux.html)

- Recommend [Visual Studio Code](https://code.visualstudio.com/download) for codding.

---

## Try sample project

We have build sample project for testing Kivy framework.
You can try it by clone source code [here](https://bitbucket.org/skglobal/kivy_image_translator)
and make steps in [README.md](https://bitbucket.org/skglobal/kivy_image_translator/src/master/README.md)

If you cannot access the above bicbucket, please contact us for assistance (python@sk-global.biz).
If you not friendly with bitbucket, please check session ["Clone existing project"](#clone-existing-project).

Some feature you can try in this project:

- Pick image from local
- Translate image to text by GoogleVision
- Text result was cached in local storage

---

## Create new project

- Install [Kivy project tools](https://drive.google.com/drive/folders/1-UgZskMrt8h-LMzugf-3Gexcncmq5L3h?usp=sharing) and run it.
- Fill in the required information into the fields and click `Create new project`. When completed, it's with show:

![Not found](img/kivy-project-tools.jpg)

- You can continue to follow these steps or ignore it.
- Open project folder by VSCode, check if it have folder .vscode and Workspace Setting(settings.json) have `"python.pythonPath": "<your name of env>\\Scripts\\python.exe"` (Mac is `<your name of env>/bin/python`). If you don't see, open setting and type python.pythonPath and add full path of VirtualEnv (python.exe)

---

## Clone existing project

- Open project on Bitbucket, click "Clone"

![Not found](img/clone-project.png)

- Choose type and click "Clone in Source"

![Not found](img/git-clone.png)

- Or click copy ![Not found](img/copy.png) then open Sourcetree press Ctrl+N and paste git link
- Create VirtualEnv environment in local project folder:

!!! hint
    If you created VirtualEnv before, you can skip this to save storage space.

```bash tab="Windows"
py -3 -m pip install --upgrade pip wheel setuptools
py -3 -m pip install --upgrade virtualenv
py -3 -m virtualenv .env
.\.env\Scripts\activate
```

```bash tab="Mac OS and Linux"
pip3 install --upgrade pip wheel setuptools
pip3 install --upgrade virtualenv
python3 -m virtualenv .env
source ./.env/bin/activate
```

- Install requirement packers `pip install -r requirements.txt`
    <!-- - Run command `pip freeze > requirements.txt` -->
- Open project folder by VSCode: check if it have folder .vscode and Workspace Setting(settings.json) have `"python.pythonPath": ".env\\Scripts\\python.exe"` (Mac is `.env/bin/python`). If you don't see or use this env, open setting and type python.pythonPath and add full path of your VirtualEnv (python.exe)
- Run testcase to check it all is OK

```bash
pytest
```

- Read README.md of project for more detail.

---

## Kivy support

- Email : kivy-users@googlegroups.com
- Google Group : [kivy-users](https://groups.google.com/group/kivy-users)
- IRC channel:
    - Server : irc.freenode.net
    - Port : 6667, 6697 (SSL only)
    - Channel : #kivy
- Kivy's discord channel   [![Discord](https://img.shields.io/badge/chat-join%20now-7289DA.svg?style=flat-square&logo=discord)](https://discordapp.com/invite/eT3cuQp)

<!-- <a href="https://sk-global.biz" title="sk-global.biz" class="md-source-file">
    sk-global.biz</a> -->
