# Game Development

## KivEnt

[**Main page**](http://www.kivent.org/)

An entity-component architecture is used to control game object state and the logic of processing the game objects.

KivEnt is built with a modular architecture and designed to have both a python api and a c-level cython api that allows more performant access to your game data. An entity-component architecture is used to control game object state and the logic of processing the game objects.

!!! info
    Can run all platform Kivy support, except iOS (issue [#360](https://github.com/kivy/kivy-ios/issues/360)).

!!! warning
    There are currently no updates and bug fixes, so **DO NOT USE IT**. ([Known issues](./known-issues-kivent.md))

If you want to build, read this:

- On Android, you can build by cloning [SkGlobal's branch](https://github.com/Thong-Tran/python-for-android/tree/update-kivent). (tested on Mac)
- On Windows, you must remove [extra_compile_args](https://github.com/kivy/kivent/blob/4709f071d56064328366df97bf8d97763b3f5d18/modules/core/setup.py#L144-L150) and use "Microsoft C++ Build tools" to build kivent_core. Other modules use mingw32. (read [this](https://groups.google.com/forum/#!topic/kivent/aHYuourrFoY) to more info)

```python
def build_ext(ext_name, files, include_dirs=[]):
    return Extension(ext_name, files, global_include_dirs + include_dirs,
                    # extra_compile_args=[cstdarg, '-ffast-math'] + extra_compile_args,
                    libraries=libraries, extra_link_args=extra_link_args,
                    library_dirs=library_dirs)
```

!!! tips "Quick try"
    [Windows wheels](https://github.com/PureAsbestos/KivEnt-Wheels-Win64-Py3.6)

## Physical Calculations

We will use [Chipmunk 2D](https://chipmunk-physics.net/) with a python wrapper for it.

### Pymunk

[![PyPI](https://img.shields.io/pypi/v/pymunk.svg)](http://www.pymunk.org)

A python wrapper for Chipmunk.

!!! info
    Can run all platform Kivy support, except iOS.

### Cymunk

A cython wrapper for Chipmunk.

!!! info
    Can run all platform Kivy support, but too old (last update on 08/09/2017) and less feature complete.

!!! warning
    On iOS, kivy-ios will build fail, you should use [SkGlobal's branch](https://github.com/Thong-Tran/kivy-ios/tree/fix-errors) to build. (tested on Mac)

### Compare these two physics libraries

| Libraries       | Get   | Collision-Callback |
| --------------- | ----- | ------------------ |
| Pymunk 5.3      | 2.14s | 3.71s              |
| Cymunk 20170916 | 0.41s | 0.95s              |

[More here](https://pymunk.readthedocs.io/en/latest/benchmarks.html#compared-to-other-physics-libraries)

## Tiled Maps & Tile Based Movement

Get up and running with a simple Tiled map.
[Kivy wiki](https://github.com/kivy/kivy/wiki/Tiled-Maps-&-Tile-Based-Movement)
