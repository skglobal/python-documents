<h1>Coding style</h1>

***Base on [PEP8](https://pep8.org/)***

**General Notes**:

- The number of spaces a tab always is **4 spaces**.
- Limit all lines to a maximum of 79 characters.
- Whitespace in expressions and statements must be reasonable.
- Always make a priority of keeping the comments up-to-date when the code changes!
- Write comments (best is docstrings) for anything will be **reused** or used in **so many places**. (e.g. the base project, UI packages, ...)
- Block comments are indented to the same level as that code. Each line of a block comment starts with a '\#' and a single space.
- The following table shows you some general guidelines on how to name your identifiers:

| Identifier                     | Convention                     |
| :----------------------------- | :----------------------------- |
| Module                         | lowercase                      |
| Class                          | CapWords                       |
| Global Variable                | lower\_case\_with\_underscores |
| Functions                      | lower\_case\_with\_underscores |
| Methods and Instance Variables | lower\_case\_with\_underscores |
| Internal using                 | \_single\_leading\_underscore  |
| Type variables                 | CapWords                       |
| Constants                      | UPPER\_CASE\_WITH\_UNDERSCORES |
| Package                        | lowercase                      |

**Exceptions that you can ignore :)**:
