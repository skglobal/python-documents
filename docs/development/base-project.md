# Base project

A basic project structure for developing the Kivy app ([view on Github](https://github.com/Thong-Tran/kivy-base-app))

**Project folder**
<pre><code>.
├─ {shortened name of project}[^1]:
│   ├─ platforms[^2]
│   ├─ uix[^3]
│   ├─ tools[^4]
│   ├─ data[^5]:
│   │   ├─ img
│   │   ├─ audio
│   │   ├─ font
│   ├─ \_\_init\_\_.py
│   ├─ app.py[^6]
│   ├─ main-layout.kv[^7]
│   ├─ screen1.py
│   ├─ screen1.kv
│   ├─ ...
├─ tests[^8]:
│   ├─ \_\_init\_\_.py
│   ├─ testcase1.py
├─ buildtools[^9]:
│   ├─ \_\_init\_\_.py
│   ├─ create-installer.iss
│   ├─ msgfmt.py
│   ├─ pygettext.py
├─ utils[^10]:
│   ├─ \_\_init\_\_.py
│   ├─ platform.py
│   ├─ resolution.py
├─ java[^11]
├─ objc[^12]
├─ main.py[^13]
├─ requirements.txt[^14]
├─ buildozer.spec[^15]
├─ desktop.spec[^16]
├─ config.ini[^17]
├─ .gitignore
├─ README.md
</code></pre>

[^1]: your project folder: keep all pages and assets of app (app content).
[^2]: platforms: customize code for access to native functions such as: facebook/google login, ads, ... You can test your module (code script) on native IDE (android studio, xcode) before add to python project.
[^3]: uix: keep customize widget, which can be reused in many different screens. Such as: appbar_custom, button_custom, ... save your time to build layout
[^4]: tools: keep tools for debug layout, view tree layout structor, multi languages.
[^5]: data: keep your assets to use in app such as: images, audios, fonts, ...
[^6]: app.py: define app class, which connect to first page.
[^7]: main-layout.kv: main ui of app, it was writen by Kivy language (same css). Use can find more information [here](https://kivy.org/doc/stable/api-kivy.html)
[^8]: tests: keep your testcase. You can define and run automation test.
[^9]: buildtools: create-installer.iss: script tool for zip windows app
[^10]: utils: some scripts rerun before app start. The purpose of this is to check the app's compatibility.
[^11]: java: folder keep java native code. This code was called via hyper-code python.
[^12]: objc: folder keep object c native code. This code was called via hyper-code python.
[^13]: main.py: start point for app, which call app define above.
[^14]: requirements.txt: keep all libs define use in app. You MUST be run cmd "pip install -r requirements.txt" before build project.
[^15]: buildozer.spec: keep build config for android/ios app, such as: app name, bundleID, app version, app code, ...
[^16]: desktop.spec: keep build config for windows/mac app
[^17]: config.ini: app config

## Features

- Multi-language support
- TextInput IME for CJK
- Enable HiDPI on Windows
- Get visible frame on Windows, Mac
- AdMob for Android, iOS
- Ads for all platforms ([buysellads](http://buysellads.com))
- Exception handler
- Ready to build for Windows, Mac, iOS, Android
- Create installer for Windows, Mac
- 'Sign in with Facebook' in app

## Controlling the environment

View more on: [Kivy environment](https://kivy.org/doc/stable/guide/environment.html)

- Restrict core to specific implementation:
    - window: egl_rpi, sdl2, pygame, sdl, x11
    - text: pil, sdl2, pygame, sdlttf
    - video: gstplayer, ffpyplayer, null (should use ffpyplayer)
    - audio: gstplayer, pygame, ffpyplayer, sdl2, avplayer (should use ffpyplayer)
    - image: tex, imageio, dds, sdl2, pygame, pil, ffpy, gif,
    - camera: opencv, gi, avfoundation, android, picamera
    - spelling: enchant, osxappkit,
    - clipboard: android, winctypes, xsel, xclip, dbusklipper, nspaste, sdl2, pygame, dummy, gtk3

## Multi-language support

- To support multi-language, add ObservableTranslation to app.py:

```python
from .tools.language import ObservableTranslation

class YourApp(App):
    tr = ObservableTranslation('en', 'yourmodule')
    . . .
```

- Tag all text:

```kv
#:set tr app.tr

Label:
    text: tr._('History')
```

- Collect tag language (like `_('text')`):

```bash
# For Mac or Linux
xgettext -Lpython --files-from=list_files --output-dir=yourmodule/po --output=messages.pot
msgmerge --update --no-fuzzy-matching --backup=off yourmodule/po/ja.po yourmodule/po/messages.pot

# For Windows
python buildtools/pygettext.py -o yourmodule/po/en.po yourmodule
```

- Compile file language(.po):

```bash
python buildtools/msgfmt.py -o yourmodule/data/locales/ja/LC_MESSAGES/lang.mo yourmodule/po/ja.po
```

- Change language:

```python
app.tr.switch_lang('ja')
```

## AdMobs

### Android

- Set buildozer.spec:

```
android.permissions = INTERNET,ACCESS_NETWORK_STATE
android.add_src = %(source.dir)s/java
android.gradle_dependencies = com.google.android.gms:play-services-ads:17.2.0
android.meta_data = com.google.android.gms.ads.APPLICATION_ID=ca-app-pub-3940256099942544~3347511713
```

- To show banner, run this code:

```python
from jnius import autoclass
AdmodSupport = autoclass('biz.sk_global.admod.AdmodSupport')

native_ads = AdmodSupport('ca-app-pub-3940256099942544~3347511713')
native_ads.newBanner('ca-app-pub-3940256099942544/6300978111', False)
native_ads.requestBanner(None)
native_ads.showBanner()
```

### iOS:

- Download and extract file: [AdMobs framework](http://dl.google.com/googleadmobadssdk/googlemobileadssdkios.zip)
- Open XCode and right-click to `Framework` folder, click `Add Files to "..." ...`
- Click `Copy items if needed` and add all framework files just downloaded.
- Add all files in `<project folder>/objc` to `Classes` folder with the same steps as above.
- Open file `Resources/wordsfinder-Info.plist`, add:

```
<key>GADApplicationIdentifier</key>
<string>ca-app-pub-3940256099942544~1458002511</string>
```

- To show banner, run this code:

```python
from pyobjus import autoclass
native_ads = autoclass('KivyAdmob').alloc().init_("ca-app-pub-3940256099942544~1458002511")
native_ads.addAdmodBanner_("ca-app-pub-3940256099942544/2934735716")
```

## Update base project

Notes when upgrading base projects:

- Please comment all features to be added or edited
