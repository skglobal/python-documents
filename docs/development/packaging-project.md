# Packaging project for platforms

[***PyInstaller***](https://pyinstaller.readthedocs.io) for Windows, Mac OS

[![PyPI](https://img.shields.io/pypi/v/pyinstaller.svg?style=for-the-badge)](https://github.com/pyinstaller/pyinstaller)

[***Buildozer***](https://github.com/kivy/buildozer) for Android, iOS

[![PyPI](https://img.shields.io/pypi/v/buildozer.svg?style=for-the-badge&logo=kivy)](https://pypi.org/project/buildozer/)

!!! note
    - Buildozer need sh module and pbxproj module for Mac OS. You must install it manually. `pip install sh pbxproj requests`
    - Most of the problems when packaging projects are missing files needed. Before build, make sure you have listed all the files you need for app. (credential file .pem, c libary .dll .so, ...)
    - Shell script to build FFmpeg for Android, iOS: [github](https://github.com/tanersener/mobile-ffmpeg)

**Device to package**

- Windows: packaging for Windows, Android ([use WSL](/#use-wsl-to-build-android)).
- Mac OS: packaging for Android, iOS, Mac OS.
- Linux(x86-64): packaging for Android, Linux.

***Sk-global projects have instructions for packaging in README.md, please follow steps in that. Because it has been optimized for that project itself.***

## Windows

To packaging app, please follow those steps in [Kivy official page](https://kivy.org/doc/stable/guide/packaging-windows.html).

**Notes when packaging:**

- Have a problem with C++ Build tools read this: [Microsoft C++ Build tools](https://blogs.msdn.microsoft.com/pythonengineering/2016/04/11/unable-to-find-vcvarsall-bat/)
- [Packer Pyenchant](https://github.com/rfk/pyenchant) is no longer maintained and have a problem when install on windows. So when you want to package the program and have this problem, use the branched kivy version of the company.
- All kv files must be embedded in program when packaging. (Because of making debug easier, UI are written in a separate file)
- Should define all environment variables before packaging.
- Install one of all requirement packers of Kivy. ([View this](../base-project/#controlling-the-environment))
- Create a hook generator for package if the defaut hook don't see. (to check that [view on kivy website](https://kivy.org/doc/stable/api-kivy.tools.packaging.pyinstaller_hooks.html#hook-generator))
<!-- - Must have import modules in \_\_init\_\_.py. -->
- You must close all app and the opened file or folder in dist folder before packaging app. If you not, pyinstaller can't build project.

### Create installer

!!! note "Download [Inno Setup](http://www.jrsoftware.org/isinfo.php)"
    ![Not found](../img/download-innosetup.png)

- Launch the Inno Setup Compiler app.
- In the welcome prompt, select Create a new script file using the Script Wizard. Click OK.
- It will open the Inno Setup Script Wizard. Click Next.
- Enter your Application Name and Application Version. Optionally, you can also include Application Publisher and Application Website details. Click Next.
- Select the Destination base folder, which defaults to Program Files. Enter an Application folder name, which is the name of main directory where your installation files will go. Click Next.
- For Application main executable file, browse and select the main EXE file that will launch your app. If you aren’t installing an app, enable 'The application doesn’t have a main executable file'. Then add main folder that contain all files and folders to your installation with 'Add folders…' buttons. Click Next.
- On the Application Shortcuts page, leave the defaults or change them to fit your preferences. They’re all self-explanatory. Click Next.
- On the Application Documentation page, you can point to up to three TXT files that will be displayed throughout the end user’s installation process. Typically these will be LICENSE.TXT, INSTALL.TXT, and README.TXT, but they can be whatever you want. Click Next.
- On the Setup Languages page, keep English but feel free to add as many other languages as you wish. Click Next.
- On the Compiler Settings page, you can customize the installer EXE file:
    - Custom compiler output folder is where the resulting installer EXE file will be placed.
    - Compiler output base file name is what the EXE file will be called. The default setting is setup.exe.
    - Custom Setup icon file is the icon that will be used for the installer EXE file. This must be an ICO file, which you can download or converted from PNG.
- Setup password will protect unauthorized users from using your installer. Leave it blank to disable this feature.
- Click Finish. When prompted to compile the new script, click Yes. When prompted to save the script, select No if this is a one-time installer file or select Yes if you plan to modify or update it later. Done!

***

## Android

Build tools: [Python for Android](https://github.com/kivy/python-for-android)
![PyPI](https://img.shields.io/pypi/v/python-for-android.svg) - [Recipes](https://github.com/kivy/python-for-android/tree/master/pythonforandroid/recipes)

To packaging app, please follow those steps in [Kivy official page](https://kivy.org/doc/stable/guide/packaging-android.html).

**Notes when packaging:**

<!--
- On Mac, install python 3.5 (on device) and use CrystaX python3 to build [view here](https://python-for-android.readthedocs.io/en/latest/buildoptions/#crystax-python3), currently SSL/TLS is broken with Python 3 [issues #1372](https://github.com/kivy/python-for-android/issues/1372).
- On Linux, install python 2.7 (on device) and use python 2.7 for build Android ([python-for-android build options](https://python-for-android.readthedocs.io/en/latest/buildoptions/#python-versions))
- Use Cython version 0.28.2, newer may cause some errors.
- ~~Use Android NDK Revision r17c. (arm-linux-androideabi-gcc is used to build project, but it removed in NDK r18)~~ **Use [Android NDK Revision r14](https://gist.github.com/roscopecoltran/43861414fbf341adac3b6fa05e7fad08) or [CrystaX NDK](https://www.crystax.net/en/download)**
- At the beginning when the download is finished, android sdk have error, run `~/.buildozer/android/platform/android-sdk-20/tools/android` to update sdk tools then exit and open again. Now you can choose version of API (in API select item: SDK Platform). -->

- Currently can't pack on Mac ([follow this issue](https://github.com/kivy/python-for-android/pull/1682))
- Filter for adb logcat: `pythonutil:I PythonActivity:I SDL:I python:I *:S`
- Module [platform](https://docs.python.org/3/library/platform.html) don't support for Android. Use [kivy.utils.platform](https://kivy.org/doc/stable/api-kivy.utils.html?highlight=platform#kivy.utils.platform) instead.
- python-for-android don't auto detect modules which needed for parent module and you have to enter manually.

    !!! tip
        You can run `pip install <missing module> --target=./.buildozer/applibs` to fix this.

- If you get error `file name too long`, delete dists folder in `./.buildozer/android/platform/build`.

### Kivy Android Virtual Machine <small>- [view official](https://kivy.org/doc/stable/guide/packaging-android-vm.html)</small>

!!! Warning
    It's out of date use [WSL](/#use-wsl-to-build-android) instead.

**Firt setup**

- Before first run VM, you should set Shared folders. (note that you must check 'Permanent' and 'Auto-mount' options)
- Open file `/etc/apt/sources.list` via 'Mousepad' and replace content on this by:

```bash
deb http://old-releases.ubuntu.com/ubuntu/ zesty main restricted universe multiverse
deb http://old-releases.ubuntu.com/ubuntu/ zesty-updates main restricted universe multiverse
deb http://old-releases.ubuntu.com/ubuntu/ zesty-security main restricted universe multiverse
deb http://old-releases.ubuntu.com/ubuntu/ zesty-backports main restricted universe multiverse
```

- Upgrade Buildozer.
- Then run `buildozer path/to/spec/file` or `buildozer init` and `buildozer android debug` to install requirement packs.

**Notes**

- Currently, VirtualBox doesn't allow symlink anymore in a shared folder. Adjust your buildozer.spec to build outside the shared folder. Also, ensure the kivy user is in the vboxsf group.
- If packaging slow, set processors of VM to max of real cores (max of logical processors not recommend)

***

## Mac OS

To packaging app, please follow those steps in [Kivy official page](https://kivy.org/doc/stable/guide/packaging-osx.html).

**Notes when packaging:**

- All content of app will pack in .app file, so we must change current working path to sys.path.

```python
if platform == 'macosx' \
    and not any(os.path.exists(i) for i in ['.Python', 'main.py']):
    for i in sys.path:
        if os.path.exists(os.path.join(i, '.Python')):
            os.chdir(i)
            break
```

- Mac OS use .icns icon file, not .ico file.
- Pyinstaller sometime not copy Tk, Tcl library. You must add that in spec file.

```python
import platform

if platform.system() == 'Darwin':
    binaries = [
    ('/System/Library/Frameworks/Tk.framework/Tk', 'tk'),
    ('/System/Library/Frameworks/Tcl.framework/Tcl', 'tcl')
    ]
else:
    binaries = []

a = Analysis(['main.py'],
            pathex=['.'],
            binaries=binaries,
)
```

### Building and signing a *.pkg file from an *.app file

<!-- !!! note
    To packaging and signing, [read this](https://docs.centrify.com/Content/Applications/AppsMobile/iOSBuildSignPkg_App.htm) -->

- To create .pkg file, run cmd `pkgbuild --install-location /Applications --component '<your app name>.app' '<your app name>.pkg'`
<!-- - To sign a component package, invoke pkgbuild with the `--sign identityName` option where identityName is the common name (CN) of the desired certificate. -->

***

## iOS

Build tools: [Kivy iOS](https://github.com/kivy/kivy-ios) - [Recipes](https://github.com/kivy/kivy-ios/tree/master/recipes)

To packaging app, please follow those steps in [Kivy official page](https://kivy.org/doc/stable/guide/packaging-ios.html).

**Notes when packaging:**

- Make sure you update final XCode version.
- For XCode 9 and newer, buildozer will show error `xcodebuild: error: invalid option '-exportFormat'`([to follow this issue](https://github.com/kivy/buildozer/issues/559)). So after run `buildozer ios debug`, you run cmd `buildozer ios xcode` and build by XCode.

!!! tip
    Or you can clone Kivy iOS and create XCode project manually, according to its instructions.

- Python on iOS can't import module from private folder `/private/var/containers/Bundle/Application`. So you must add path `/var/containers/Bundle/Application/` to `sys.path`.

```c tab="Objective-C (main.m)"
NSString *python_path = [NSString stringWithFormat:@"PYTHONPATH=%@:%@/lib/python3.7/:%@/lib/python3.7/site-packages:%@/YourApp:%@/YourApp/_applibs", resourcePath, resourcePath, resourcePath, resourcePath, resourcePath, nil];
```

```python tab="Python"
if platform == 'ios':
    for i in sys.path:
        if os.path.exists(os.path.join(i, 'YourApp', '_applibs')) and \
            i.startswith('/var/containers'):
            sys.path += [os.path.join(i, 'YourApp'),
                         os.path.join(i, 'YourApp', '_applibs')]
```

- When have a problem to find 'iphonesimulator' SDK file when creating iOS project, you should reinstall [Command line tools for XCode](https://developer.apple.com/download/more/?=for%20Xcode)

```bash
xcrun: error: SDK "iphonesimulator" cannot be located
xcrun: error: SDK "iphonesimulator" cannot be located
xcrun: error: unable to lookup item 'Path' in SDK 'iphonesimulator'
```

- When have SSL Error. Navigate to `/Applications/Python x.x/`, click on `Install Certificates.command` and this should solve it.

```bash
ssl.SSLCertVerificationError: [SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1056)
```

<!-- - xcodebuild command `-exportformat ipa` is no longer supported. So we use `-exportOptionsPlist ${EXPORTOPTIONSPLISTPATH}` instead.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>provisioningProfiles</key>
    <dict>
        <key>MY_APP_BUNDLE_ID</key>
        <string>MY_PROFILE_NAME_AS_SHOWN_BY_XCODE or UUID_FOUND_IN_MOBILEPROVISION_FILE</string>
    </dict>
    <key>signingCertificate</key>
    <string>iOS Distribution</string>
    <key>signingStyle</key>
    <string>manual</string>
    <key>teamID</key>
    <string>MY_TEAM_ID</string>
</dict>
</plist>
``` -->

- You must delete .so.o when upload app on AppStore ([to follow this issue](https://github.com/kivy/kivy-ios/issues/315))
- To fix a "Duplicated Symbols" error on binary files [read this](http://angelolloqui.com/blog/31-How-to-fix-a-Duplicated-Symbols-error-on-binary-files) or [this](https://github.com/kivy/kivy-ios/blob/c2c552e7a9dc3827dc5923475d5f2b9986b5db7f/tools/environment.sh#L116).

***

## Linux

Use [**setup.py**](https://docs.python.org/3.7/distutils/setupscript.html) to package project.

**Notes when packaging**

- If you have error with `Unable to find any valuable Cutbuffer provider at all! xclip and xsel: No such file or directory`, run cmd `sudo apt-get install xclip xsel`
