# Known issues of KivEnt:

## All

8_simple_animation and 12_drawing_shapes when click "Clear"

```
Exception ignored in: 'kivent_core.rendering.fixedvbo.FixedVBO.reload'
 TypeError: 'NoneType' object is not callable
```

### 9_twinkling_stars

```
 Exception ignored in: 'kivent_core.rendering.fixedvbo.FixedVBO.reload'
 TypeError: 'NoneType' object is not callable
[ERROR  ] OpenGL Error (FixedVBO.update_buffer-glBufferSubData) 1281 / 501 for None
```

## Windows

### 13_tilemap

```log
 Traceback (most recent call last):
   File ".\main.py", line 106, in <module>
     YourAppNameApp().run()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\app.py", line 826, in run
     runTouchApp()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\base.py", line 502, in runTouchApp
     EventLoop.window.mainloop()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\core\window\window_sdl2.py", line 727, in mainloop
     self._mainloop()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\core\window\window_sdl2.py", line 460, in _mainloop
     EventLoop.idle()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\base.py", line 337, in idle
     Clock.tick()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\clock.py", line 581, in tick
     self._process_events()
   File "kivy\_clock.pyx", line 384, in kivy._clock.CyClockBase._process_events
   File "kivy\_clock.pyx", line 414, in kivy._clock.CyClockBase._process_events
   File "kivy\_clock.pyx", line 412, in kivy._clock.CyClockBase._process_events
   File "kivent_core/gameworld.pyx", line 275, in kivent_core.gameworld.GameWorld.init_gameworld
   File ".\main.py", line 28, in init_game
     self.setup_tile_map()
   File ".\main.py", line 85, in setup_tile_map
     map_manager.load_map('my_map', 100, 100, tiles)
   File "kivent_maps\map_manager.pyx", line 84, in kivent_maps.map_manager.MapManager.load_map
   File "kivent_maps\map_data.pyx", line 293, in kivent_maps.map_data.TileMap.__cinit__
   File "kivent_core/memory_handlers/block.pyx", line 53, in kivent_core.memory_handlers.block.MemoryBlock.allocate_memory_with_buffer
   File "kivent_core/memory_handlers/membuffer.pyx", line 148, in kivent_core.memory_handlers.membuffer.Buffer.add_data
 MemoryError
```

### 14_tmx_loader

```
 Traceback (most recent call last):
   File ".\main.py", line 112, in <module>
     YourAppNameApp().run()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\app.py", line 826, in run
     runTouchApp()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\base.py", line 502, in runTouchApp
     EventLoop.window.mainloop()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\core\window\window_sdl2.py", line 727, in mainloop
     self._mainloop()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\core\window\window_sdl2.py", line 460, in _mainloop
     EventLoop.idle()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\base.py", line 340, in idle
     self.dispatch_input()
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\base.py", line 325, in dispatch_input
     post_dispatch_input(*pop(0))
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\base.py", line 231, in post_dispatch_input
     listener.dispatch('on_motion', etype, me)
   File "kivy\_event.pyx", line 707, in kivy._event.EventDispatcher.dispatch
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\core\window\__init__.py", line 1360, in on_motion
     self.dispatch('on_touch_down', me)
   File "kivy\_event.pyx", line 707, in kivy._event.EventDispatcher.dispatch
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\core\window\__init__.py", line 1376, in on_touch_down
     if w.dispatch('on_touch_down', touch):
   File "kivy\_event.pyx", line 703, in kivy._event.EventDispatcher.dispatch
   File "kivy\_event.pyx", line 1214, in kivy._event.EventObservers.dispatch
   File "kivy\_event.pyx", line 1098, in kivy._event.EventObservers._dispatch
   File "C:\Users\LAPTOP MSI\Documents\.env37\lib\site-packages\kivy\lang\builder.py", line 64, in custom_callback
     exec(__kvlang__.co_value, idmap)
   File "C:\Users\LAPTOP MSI\Documents\kivent\examples\14_tmx_loader\yourappname.kv", line 6, in <module>
     on_touch_down: self.screen_touched(args[1])
   File ".\main.py", line 93, in screen_touched
     print('Tile %d,%d clicked' % self.tilemap.get_tile_index(x,y))
   File "kivent_maps\map_data.pyx", line 640, in kivent_maps.map_data.StaggeredTileMap.get_tile_index
 UnboundLocalError: local variable 'col' referenced before assignment
```

### 16_svg_phys_objects

```log
 .\main.py:85: DeprecationWarning: This method will be removed in future versions.  Use 'list(elem)' or iteration over elem instead.
   data = mm.get_model_info_for_svg(fname)
Aborting due to Chipmunk error: Polygon is concave or has a reversed winding. Consider using cpConvexHull() or CP_CONVEX_HULL().
        Failed condition: cpPolyValidate(verts, numVerts)
        Source:C:\Users\LAPTOP~1\AppData\Local\Temp\pip-req-build-3m4iyo2k\cymunk\Chipmunk-Physics\src\cpPolyShape.c:215
```

## Android

Error when open .svg file

```log
I/python  (15250):  Traceback (most recent call last):
I/python  (15250):    File "main.py", line 60, in <module>
I/python  (15250):      app.run()
I/python  (15250):    File "/data/data/skglobal.kivent/files/app/_python_bundle/site-packages/kivy/app.py", line 826, in run
I/python  (15250):      runTouchApp()
I/python  (15250):    File "/data/data/skglobal.kivent/files/app/_python_bundle/site-packages/kivy/base.py", line 502, in runTouchApp
I/python  (15250):      EventLoop.window.mainloop()
I/python  (15250):    File "/data/data/skglobal.kivent/files/app/_python_bundle/site-packages/kivy/core/window/window_sdl2.py", line 727, in mainloop
I/python  (15250):      self._mainloop()
I/python  (15250):    File "/data/data/skglobal.kivent/files/app/_python_bundle/site-packages/kivy/core/window/window_sdl2.py", line 460, in _mainloop
I/python  (15250):      EventLoop.idle()
I/python  (15250):    File "/data/data/skglobal.kivent/files/app/_python_bundle/site-packages/kivy/base.py", line 337, in idle
I/python  (15250):      Clock.tick()
I/python  (15250):    File "/data/data/skglobal.kivent/files/app/_python_bundle/site-packages/kivy/clock.py", line 581, in tick
I/python  (15250):      self._process_events()
I/python  (15250):    File "kivy/_clock.pyx", line 384, in kivy._clock.CyClockBase._process_events
I/python  (15250):    File "kivy/_clock.pyx", line 414, in kivy._clock.CyClockBase._process_events
I/python  (15250):    File "kivy/_clock.pyx", line 412, in kivy._clock.CyClockBase._process_events
I/python  (15250):    File "kivy/_clock.pyx", line 167, in kivy._clock.ClockEvent.tick
I/python  (15250):    File "kivent_core/gameworld.pyx", line 278, in kivent_core.gameworld.GameWorld.init_gameworld.lambda
I/python  (15250):    File "kivent_core/gameworld.pyx", line 272, in kivent_core.gameworld.GameWorld.init_gameworld
I/python  (15250):    File "kivent_core/gameworld.pyx", line 221, in kivent_core.gameworld.GameWorld.allocate
I/python  (15250):    File "kivent_core/memory_handlers/membuffer.pyx", line 103, in kivent_core.memory_handlers.membuffer.Buffer.allocate_memory
I/python  (15250):  MemoryError
```
