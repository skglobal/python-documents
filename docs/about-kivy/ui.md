# User Interface

Kivy provides a design language specifically geared towards easy and scalable GUI Design. The language makes it simple to separate the interface design from the application logic, adhering to the [separation of concerns principle](http://en.wikipedia.org/wiki/Separation_of_concerns).

```kivy
<LoginScreen>:  # every class in your app can be represented by a rule like
    #             this in the kv file
    Button: # this is how you add your widget/layout to the parent
        #     (note the indentation).
        font_name: 'Roboto-Regular.ttf'# to set each property of your widget/layout
        text:
            # add code one line and line break
            '{} {}'.format(app.last_name, app.first_name)\
            if app.have_name else ''
        on_release:
            '''
            for event (on_<propname>)
            you can add code like normal
            '''
            from kivy.logger import Logger
            Logger.info('Run release')
            # but cannot indent
            app.fbAcc.get_token() if not app.fbAcc.token else None
```

That’s it, that’s how simple it is to design your GUI in the Kv language. For a more in-depth understanding, please refer to the [Kv language](https://kivy.org/doc/stable/guide/lang.html) and [API](https://kivy.org/doc/stable/api-kivy.lang.html) documentation.

You can try tutorial step-by-step in [Kivy official page](https://kivy.org/doc/stable/tutorials/pong.html).

## Notes

- To change default font:

```python tab="Use config.ini"
[kivy]
default_font = ['<Font name>',
    '/path/to/file.ttf',
    '/path/to/file.ttf',
    '/path/to/file.ttf',
    '/path/to/file.ttf']
```

```python tab="Use code"
from kivy.core.text import LabelBase
font = {
    "name": "Roboto",
    "fn_regular": '/path/to/file.ttf',
    "fn_bold": '/path/to/file.ttf',
    "fn_italic": '/path/to/file.ttf',
    "fn_bolditalic": '/path/to/file.ttf'
}
LabelBase.register(**font)
```

```python tab="Use KivyMD of SK-Global (not changed completely)"
from kivy.config import Config
Config.set('myapp', 'default_font_style', '<your font style>')
Config.set('myapp', 'list_fonts',
    {'<your font style>':
        # Font path, Bold, Mobile size, Desktop size (None if same as Mobile)
        ['/path/to/file.ttf', False, 14, 13]
})
```

- You cannot do any OpenGL-related work in a thread (create widget, graphic, ...), so you must wrap code in [mainthread](https://kivy.org/doc/stable/api-kivy.clock.html?highlight=clock#kivy.clock.mainthread) or schedule a function call in the future by [Clock](https://kivy.org/doc/stable/api-kivy.clock.html).

```python
@mainthread
def callback(self, *args):
    print('The request succeeded!',
          'This callback is called in the main thread.')


self.req = UrlRequest(url='http://...', on_success=callback)
```

- Use method load file `Builder.load_file('name_widget.kv')` to put Kivy language into the program instead of write in python code `Builder.load_string(KV_CODE)`. Because it is the least errors and will show error popup inline in VS Code.
- Currently, there isn't IDE good support for Kivy language.
- If you get import error in .kv file, be sure folder contain py file have \_\_init\_\_.py and have code `from .pyfile import MyClass`.

## Public themes

We can use avaible theme, which design by Meterial.

- [Google's Material Design](https://gitlab.com/kivymd/KivyMD)
    - Fork from KivyMD:
        - [HeaTTheatR](https://github.com/HeaTTheatR/KivyMD) (not keep git history when forking, but have many features)
        - [AndreMiras](https://github.com/AndreMiras/KivyMD) (also keep git history but only fix errors)

Sanple layout with TextField

![Not found](../img/themes/kivymd-app-1.png)

Sample layout with listview and checkbox

![Not found](../img/themes/kivymd-app-2.png)

- Default theme builtin Kivy framework

![Not found](../img/themes/kivy.png)
![Not found](../img/themes/settingswithspinner_kivy.jpg)

- [FlatKivy](https://github.com/Kovak/FlatKivy)

![Not found](../img/themes/flatkivy.jpg)

## Own themes

- [Fork from KivyMD](https://bitbucket.org/skglobal/kivymd-skglobal)

## High DPI for python app

On Windows, must run those code before run app.

On Mac, python will run on HiDPI without any additional codes. But when you bundle app to .app file you should add some info into `Info.plist`.

On mobile, everything will work fine 😁.

```python tab="Windows: main.py"
import ctypes
shcore = ctypes.windll.shcore
shcore.SetProcessDpiAwareness(2)
```

```python tab="MacOS: pyinstaller.spec"
app = BUNDLE(coll,
        name='MyApp.app',
        bundle_identifier=None,
        info_plist={
            'NSPrincipleClass': 'NSApplication',
            'NSHighResolutionCapable': True,
            },
)
```

!!!info "Reference"
    - Windows: [stackoverflow](https://stackoverflow.com/questions/44398075/can-dpi-scaling-be-enabled-disabled-programmatically-on-a-per-session-basis/44422362#44422362)
    - MacOS: [stackoverflow](https://stackoverflow.com/questions/34465506/retina-support-in-qt5-on-os-x), [pyinstaller](https://pyinstaller.readthedocs.io/en/stable/spec-files.html#spec-file-options-for-a-mac-os-x-bundle), [SDL2](https://wiki.libsdl.org/SDL_CreateWindow)
