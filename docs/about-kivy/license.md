# License note

To know more about license:

- [In Kivy page](https://kivy.org/doc/stable/guide/licensing.html)
- [choosealicense.com](https://choosealicense.com/licenses/) and [appendix](https://choosealicense.com/appendix/)

## Kivy and core modules

- Kivy: [MIT License](https://github.com/kivy/kivy/blob/master/LICENSE)
- Pillow: [license](https://github.com/python-pillow/Pillow/blob/master/LICENSE)
- glew: [license](http://glew.sourceforge.net/glew.txt)
- pypiwin32: [PSF license](https://opensource.org/licenses/Python-2.0)
- zlib: [license](https://github.com/madler/zlib/blob/master/README)
- python on Windows: [Py3 CRT license](https://hg.python.org/cpython/file/tip/Tools/msi/exe/crtlicense.txt)
- SDL 2.0: [zlib license](https://www.libsdl.org/license.php)
- SDL Free type: [FreeType Project LICENSE](https://hg.libsdl.org/SDL_ttf/file/b357aefce885/VisualC/external/lib/x64/LICENSE.freetype.txt)
- SDL mixer: [LGPL, other license](http://hg.libsdl.org/SDL_mixer/file/default/VisualC/external/lib/x86)
- Pygments: [BSD License](https://bitbucket.org/birkenfeld/pygments-main/src/tip/LICENSE)
- docutils: BSD License, GNU General Public License (GPL), Python Software Foundation License ([view file](https://sf.net/p/docutils/code/HEAD/tree/trunk/docutils/COPYING.txt))

## Internet

- OpenSSL later v3.0.0: [Apache License v2](https://www.openssl.org/source/apache-license-2.0.txt)
- OpenSSL before v3.0.0: [OpenSSL and SSLeay license](https://www.openssl.org/source/license-openssl-ssleay.txt)
- requests: [Apache Software License 2.0](https://github.com/kennethreitz/requests/blob/master/LICENSE)
- chardet: [Lesser General Public License](https://github.com/chardet/chardet/blob/master/LICENSE)
- certifi: [Mozilla Public License 2.0](https://github.com/certifi/python-certifi/blob/master/LICENSE)
- idna: [BSD License (BSD-like)](https://github.com/kjd/idna/blob/master/LICENSE.rst)
- urllib3: [MIT License](https://github.com/urllib3/urllib3/blob/master/LICENSE.txt)

## Audio/Video modules

- FFmpeg: [LGPLv2.1](https://www.ffmpeg.org/legal.html)
- External libraries of FFmpeg: LGPL, GPL, MIT, ... ([view file](https://github.com/tanersener/mobile-ffmpeg/wiki/License-GPL-v3.0))
- gstreamer: [GNU General Public License](https://github.com/GStreamer/gstreamer/blob/master/COPYING)

## Kivy project tools

- cookiecutter: [BSD License](https://github.com/audreyr/cookiecutter/blob/master/LICENSE)

## Tetris 2D

- requests oauthlib: [BSD License (ISC)](https://github.com/requests/requests-oauthlib/blob/master/LICENSE)
- pyOpenSSL: [Apache License, Version 2.0](https://github.com/pyca/pyopenssl/blob/master/LICENSE)
