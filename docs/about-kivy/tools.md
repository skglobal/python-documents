# Tools for Kivy

!!! warning
    All the tools below are an work-in-progress and currently under construction.

## [kivy.tools](https://kivy.org/doc/master/api-kivy.tools.html)

The tools module provides various utility scripts, modules and examples.

- kviewer.py: for viewing kv files with automatic updating
- benchmark.py: provides detailed OpenGL hardware information as well as some benchmarks measuring kivy specific performance
- reports.py: provides a comprehensive report covering your systems providers, libraries, configuration, environment, input devices and options
- texturecompress.py: a command line utility for compressing images into PVRTC or ETC1 formats
- generate-icons.py: generates set of icons suitable for the various store and package formats
- gles_compat/subset_gles.py: examines compatibility between GLEXT and GLES2 headers for finding compatible subsets

Note: sample script above can found [at here](https://github.com/kivy/kivy/tree/master/kivy/tools)

## [kivy.modules](https://kivy.org/doc/master/api-kivy.modules.html)

Modules are classes that can be loaded when a Kivy application is starting. The loading of modules is managed by the config file. Currently, including:

- touchring: Draw a circle around each touch.
- monitor: Add a red topbar that indicates the FPS and a small graph indicating input activity.
- keybinding: Bind some keys to actions, such as a screenshot.
- recorder: Record and playback a sequence of events. (for auto test)
- screen: Emulate the characteristics (dpi/density/resolution) of different screens.
- inspector: Examines your widget hierarchy and widget properties. (for debug UI)
- console: Reboot of the old inspector, designed to be modular and keep concerns separated. It also has an addons architecture that allow you to add a button, panel, or more in the Console itself.
- webdebugger: Realtime examination of your app internals via a web browser.
- joycursor: Navigate in your app with a joystick.
- showborder: Show widget’s border. (for debug UI)

## [kivy.utils](https://kivy.org/doc/master/api-kivy.utils.html)

The Utils module provides a selection of general utility functions and classes that may be useful for various applications. These include maths, color, algebraic and platform functions.

## Other

- Input Method Editors for Windows: [github](https://github.com/Naotonosato/MilletPie)
- Automation for Kivy: [telenium](https://github.com/tito/telenium)
- Led widget: [github](https://github.com/olivier-boesch/garden.led)
- Image Toggle Button widget: [github](https://github.com/olivier-boesch/garden.imagetogglebutton/)
- Image progress bar: [github](https://github.com/olivier-boesch/garden.imageprogressbar)
