# Python library for accessing features of hardware / platforms

In session, we will provide a soulution for call native function from difference platform (Android, OS, Windows, Mac OS, Linux).

## Cross-platform of Kivy

**[Kivy/Plyer](https://plyer.readthedocs.io/en/latest/)**

[![PyPI](https://img.shields.io/pypi/v/plyer.svg?style=for-the-badge)](https://github.com/kivy/plyer)

Plyer is a Python library for accessing features of your hardware / platforms.
You can access Github and get latest information support at [here](https://github.com/kivy/plyer#supported-apis)

### Supported APIs

Bellow is Supported Apis table for all platform support by Kivy. You can use them directly without adding any modules.
<!-- !!!note
    View full in [here](https://github.com/kivy/plyer#supported-apis) -->

<!-- <iframe src="/about-kivy/test.html"></iframe> -->

| Platform                 | Android | iOS | Windows | Mac OS | Linux |
| ------------------------ | ------- | --- | ------- | ------ | ----- |
| Notifications            | X       |     | X       | X      | X     |
| Wifi                     |         |     | X       | X      | X     |
| Bluetooth                | X       |     |         | X      |       |
| GPS                      | X       | X   |         |        |       |
| Accelerometer            | X       | X   |         | X      | X     |
| Gyroscope                | X       | X   |         |        |       |
| Compass                  | X       | X   |         |        |       |
| Barometer                | X       | X   |         |        |       |
| Battery                  | X       | X   | X       | X      | X     |
| Vibrator                 | X       | X   |         |        |       |
| Brightness               | X       | X   |         |        | X     |
| Call                     | X       | X   |         |        |       |
| Camera (taking picture)  | X       | X   |         |        |       |
| Email (open mail client) | X       | X   | X       | X      | X     |
| Native file chooser      | X       |     | X       | X      | X     |
| Screenshot               |         |     | X       | X      | X     |
| Sms (send messages)      | X       | X   |         |        |       |
| Storage Path             | X       | X   | X       | X      | X     |

---

## from Python import Native

In case the above apis is not enough for your needs. You can write specific functions with native code,
call these functions through the modules below (for Android and iOS)

- [**PyJNIus**](https://pyjnius.readthedocs.io) for Android (dynamic access to Java).

<!-- [![PyPI](https://img.shields.io/pypi/v/pyjnius.svg)](https://github.com/kivy/pyjnius) -->

- [**Pyobjus**](https://pyobjus.readthedocs.io) for iOS (dynamic access to Objective C).

<!-- [![PyPI](https://img.shields.io/pypi/v/pyobjus.svg)](https://github.com/kivy/pyobjus) -->

- For Windows and MacOS, we can build lib native code by C/C#/Objective C and call these libs through python code.([ctypes](https://docs.python.org/3/library/ctypes.html), [cffi](https://cffi.readthedocs.io))

<!-- Note: This section requires that you have experience with native code. Complex configuration can be frustrating. However, this is an extremely important part for your applications. -->

**Examples**

### Java array in Python

```python tab="Android"
from jnius import autoclass
jString = autoclass('java.lang.String')
jArray = autoclass("java.lang.reflect.Array")
a = jArray.newInstance(jString, 10)
a
# [None, None, None, None, None, None, None, None, None, None]
a[0] = jString("nitish")
a
# [<java.lang.String at 0x103d619b0 jclass=java/lang/String jself=<LocalRef obj=0x7fc665f4a3f8 at 0x103abf190>>, None, None, None, None, None, None, None, None, None]
```

### Ads

!!! note "Reference"
    http://quadropoly.com.au/kivy-and-admob-for-android-api-27/

### Choose pictures

```python tab="Android"
from jnius import autoclass
RESULT_LOAD_IMG = 1
Intent = autoclass('android.content.Intent')
jString = autoclass('java.lang.String')
Media = autoclass('android.provider.MediaStore$Images$Media')

# Get main activity of PyJNIus
activity = autoclass('org.kivy.android.PythonActivity').mActivity

# Create intent
getIntent = Intent(Intent.ACTION_GET_CONTENT)
getIntent.setType("image/*")

# Handle activity results
def handle_image_result(requestCode, resultCode, intent):
    Activity = autoclass('android.app.Activity')
    activity = autoclass('org.kivy.android.PythonActivity').mActivity
    RealPathUtil = autoclass('com.myjava.RealPathUtil')
    if (requestCode == RESULT_LOAD_IMG and resultCode == Activity.RESULT_OK
                and intent != None):
        realPath = RealPathUtil.getRealPathFromURI_API19(activity, intent.getData())

# Register and active it
activity.registerActivityResultListener(ActivityResultListener(handle_image_result))
activity.startActivityForResult(getIntent, RESULT_LOAD_IMG)
```

!!! note "Reference"
    [RealPathUtil.java](https://gist.github.com/tatocaster/32aad15f6e0c50311626)

### 'Sign in with Facebook' in Python app

- For Android, iOS should use native SDk.

!!!hint
    iOS example: [Kivy](https://blog.kivy.org/2013/08/using-facebook-sdk-with-python-for-android-kivy/), [github](https://github.com/samjhyip/Simple-Facebook-Login-on-Kivy)

- On desktop, create [a manual login Facebook flow](https://developers.facebook.com/docs/facebook-login/manually-build-a-login-flow).You can use [Requests OAuthlib](https://requests-oauthlib.readthedocs.io/) to handle OAuth FB and redirect to localhost Flask (Fb only accept https).

```python
import webbrowser
from threading import Thread
from requests_oauthlib import OAuth2Session
from requests_oauthlib.compliance_fixes import facebook_compliance_fix
from flask import Flask
from flask import request

# Credentials you get from registering a new application
client_id = 'app_id'
client_secret = 'app_secret'

# OAuth endpoints given in the Facebook API documentation
authorization_base_url = \
    ('https://www.facebook.com/v3.2/dialog/oauth?'
    'scope=email,public_profile')
token_url = \
    'https://graph.facebook.com/oauth/access_token'
redirect_uri = 'https://127.0.0.1:5000/'

fb = OAuth2Session(client_id, redirect_uri=redirect_uri)
fb = facebook_compliance_fix(fb)

app = Flask(__name__)

@app.route("/")
def hello():
    # Get shutdown func
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        func = str
        print('Server: Not running with the Werkzeug Server')

    # Get the authorization verifier code from the callback url
    redirect_response = request.url
    handle_thr = Thread(target=_handle_fb,args=(redirect_response, func))
    handle_thr.setDaemon(True)
    handle_thr.start()

    return "All done"

def _handle_fb(redirect_response, shutdown_serv):
    # Fetch the access token
    data = fb.fetch_token(token_url, client_secret=client_secret,
                            authorization_response=redirect_response)
    token = data['access_token']

    # Fetch a protected resource, i.e. user profile
    r = fb.get('https://graph.facebook.com/me?')
    print(r.content)

    print('Server: Shutting down...')
    shutdown_serv()
    thr.join(timeout=5)

# Run https server, must install pyOpenSSL to use
thr = Thread(target=app.run, kwargs={'ssl_context':'adhoc'})
thr.setDaemon(True)
thr.start()

# Redirect user to Facebook for authorization
authorization_url, state = fb.authorization_url(authorization_base_url)
# Open authorization_url
webbrowser.open(authorization_url)
```
