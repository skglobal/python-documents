# Kivy wiki of Sk-global

![](https://readthedocs.org/projects/kivy-skglobal/badge/?version=latest)

This wiki use **[MkDocs](https://www.mkdocs.org/)** to create site docs and hosting on [Read the Docs](https://readthedocs.org/). This is a public site, so please ***do not add sensitive information*** here.

## User Guide

- Install MkDocs `pip3 install mkdocs-material`
- Testing wiki site:
    - Run `mkdocs serve` and open up http://127.0.0.1:8000/ in your browser.
    - In VSCode, you have to install Powershell extension and press F5.
